package com.eriseed.calldetails.controller;

import com.eriseed.calldetails.dto.CallDetailsDTO;
import com.eriseed.calldetails.service.CallDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class CallDetailsController {

// Just sample comment
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CallDetailsService callDetailsService;

    // Fetches call details of a specific customer
    // Fetches call details of a specific customer
    // sample check in 01 nove 2019

    @RequestMapping(value="/customers/{phoneNo}/calldetails", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CallDetailsDTO> getCustomerCallDetails(@PathVariable long phoneNo){

        logger.info("Calldetails request for customer {}", phoneNo);

        return callDetailsService.getCustomerCallDetails(phoneNo);
    }
}

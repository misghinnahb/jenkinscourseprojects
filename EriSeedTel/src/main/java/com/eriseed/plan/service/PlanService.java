package com.eriseed.plan.service;

import com.eriseed.plan.dto.PlanDTO;
import com.eriseed.plan.entity.Plan;
import com.eriseed.plan.repository.PlanRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PlanService {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    PlanRepository planRepo;

    // Fetch all plan details
    public List<PlanDTO> getAllPlans() {
        List<Plan> plans = planRepo.findAll();
        List<PlanDTO> planDTOS = new ArrayList<>();

        for (Plan plan : plans) {
            PlanDTO planDTO = PlanDTO.valueOf(plan);
            planDTOS.add(planDTO);
        }
        logger.info("Plan details : {}", planDTOS);
        return planDTOS;
    }

    // To get a plan based on plan Id
    public PlanDTO getPlan(Integer planId) {
        PlanDTO planDTO = null;
        Optional<Plan> optPlan = planRepo.findById(planId);
        if (optPlan.isPresent()) {
            Plan plan = optPlan.get();
            planDTO = planDTO.valueOf(plan);
        }
        return planDTO;
    }
}

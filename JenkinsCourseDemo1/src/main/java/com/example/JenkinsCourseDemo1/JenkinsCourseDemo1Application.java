package com.example.JenkinsCourseDemo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsCourseDemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(JenkinsCourseDemo1Application.class, args);
		System.out.println("The sum of 2 and 3 is: " + 2+3)

		System.out.println("This is added to trigger a jenkins Build and additional modification");
	}

}
